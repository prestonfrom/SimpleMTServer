#pragma once

#include "utilities/utility_functions.h"
#include "simple_server/SimpleServer.h"
#include "simple_server/HttpRequest/HttpRequestEnums.h"

#include <iostream>

namespace parsing_test {
const std::string content_header_string = "Content: content_is_king";
const std::string content_header_name = "Content";
const std::string content_header_content = "content_is_king";
const std::string length_name = "Content-Length";
const std::string length_content = "1337";
const std::unordered_map<std::string, std::string> header_results{
    {"Content",        "content_is_king"},
    {"Content-Length", "1337"}
};
const std::string get_request_message = "GET /hello.htm HTTP/1.1\n"
                                        "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\n"
                                        "Host: www.notarealhost.com\n"
                                        "Accept-Language: en-us\n"
                                        "Accept-Encoding: gzip, deflate\n"
                                        "Connection: Keep-Alive\r\n\r\n";

const std::string post_request_message = "POST /hello.htm HTTP/1.1\n"
                                         "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\n"
                                         "Host: www.notarealhost.com\n"
                                         "Accept-Language: en-us\n"
                                         "Accept-Encoding: gzip, deflate\n"
                                         "Connection: Keep-Alive\r\n\r\n"
                                         "{\"a\" : \"b\"}";

const std::string post_body = "{\"a\" : \"b\"}";

const std::string get_request_with_body = "GET /hello.htm HTTP/1.1\n"
                                          "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\n"
                                          "Host: www.notarealhost.com\n"
                                          "Accept-Language: en-us\n"
                                          "Accept-Encoding: gzip, deflate\n"
                                          "Connection: Keep-Alive\r\n\r\n"
                                          "query=parameters&a=b&param=map";

const std::string query_params = "query=parameters&a=b&param=map";
const std::unordered_map<std::string, std::string> query_param_map = {
    {"query", "parameters"}, {"a", "b"}, {"param", "map"}
};

const std::string get_query_params = "GET /hello.htm?query=parameters&a=b&param=map HTTP/1.1\n"
                                     "User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\n"
                                     "Host: www.notarealhost.com\n"
                                     "Accept-Language: en-us\n"
                                     "Accept-Encoding: gzip, deflate\n"
                                     "Connection: Keep-Alive\r\n\r\n";

uint64_t test_http_method_enum() {
    auto expected_method = simple_server::HttpMethod::GET;
    auto received_method = simple_server::to_enum<simple_server::HttpMethod>("GET");

    auto result = check_and_print_results(
        expected_method,
        received_method,
        "Testing to enum template is correct -- but returned enum is wrong."
    );

    expected_method = simple_server::HttpMethod::POST;
    received_method = simple_server::to_enum<simple_server::HttpMethod>("POST");

    result += check_and_print_results(
        expected_method,
        received_method,
        "Testing to enum template is correct -- but returned enum is wrong."
    );

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

uint64_t test_parsing_headers() {
    simple_server::Headers headers;
    headers.AddHeader(length_name, length_content);
    headers.AddHeaderFromString(content_header_string);

    auto& content_header_result = headers.GetHeaders().find(content_header_name)->second;
    auto result = check_and_print_results(
        content_header_content,
        content_header_result,
        "Content header contents do not match."
    );

    auto& length_header_result = headers.GetHeaders().find(length_name)->second;
    result += check_and_print_results(
        length_content,
        length_header_result,
        "Length header content does not match."
    );

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

uint64_t test_parsing_request_headers() {
    simple_server::HttpRequest request;
    request.ParseIncomingMessage(get_request_message);
    auto result = check_and_print_results(
        simple_server::HttpMethod::GET,
        request.GetHttpMethod(),
        "HTTP method does not match"
    );

    result += check_and_print_results(
        simple_server::HttpVersion::v1_1,
        request.GetHttpVersion(),
        "HTTP version does not match"
    );

    result += check_and_print_results(
        "hello.htm",
        request.GetHttpPath(),
        "HTTP path does not match"
    );

    result += check_and_print_results(
        false,
        request.GetBody().has_value(),
        "Post body should not have a value"
    );

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

uint64_t test_parsing_post_body() {
    simple_server::HttpRequest request;
    request.ParseIncomingMessage(post_request_message);

    auto result = check_and_print_results(
        simple_server::HttpMethod::POST,
        request.GetHttpMethod(),
        "HTTP method does not match"
    );

    result += check_and_print_results(
        simple_server::HttpVersion::v1_1,
        request.GetHttpVersion(),
        "HTTP version does not match"
    );

    result += check_and_print_results(
        "hello.htm",
        request.GetHttpPath(),
        "HTTP path does not match"
    );

    if (request.GetBody().has_value()) {
        result += check_and_print_results(
            post_body,
            request.GetBody().value(),
            "Post body is not correct"
        );
    } else {
        result += check_and_print_results(
            true,
            request.GetBody().has_value(),
            "Post body should have a value"
        );
    }

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

uint64_t test_parsing_get_body() {
    simple_server::HttpRequest request;
    request.ParseIncomingMessage(get_request_with_body);

    auto result = check_and_print_results(
        simple_server::HttpMethod::GET,
        request.GetHttpMethod(),
        "HTTP method does not match"
    );

    result += check_and_print_results(
        simple_server::HttpVersion::v1_1,
        request.GetHttpVersion(),
        "HTTP version does not match"
    );

    result += check_and_print_results(
        "hello.htm",
        request.GetHttpPath(),
        "HTTP path does not match"
    );

    if (request.GetBody().has_value()) {
        result += check_and_print_results(
            query_params,
            request.GetBody().value(),
            "GET body is not correct"
        );
    } else {
        result += check_and_print_results(
            true,
            request.GetBody().has_value(),
            "GET body should have a value"
        );
    }

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

uint64_t test_parsing_get_query_params() {
    simple_server::HttpRequest request;
    request.ParseIncomingMessage(get_query_params);

    auto result = check_and_print_results(
        simple_server::HttpMethod::GET,
        request.GetHttpMethod(),
        "HTTP method does not match"
    );

    result += check_and_print_results(
        simple_server::HttpVersion::v1_1,
        request.GetHttpVersion(),
        "HTTP version does not match"
    );

    result += check_and_print_results(
        "hello.htm",
        request.GetHttpPath(),
        "HTTP path does not match"
    );

    result += check_and_print_results(
        query_params,
        request.GetHttpQueryParameterString(),
        "Query parameters string does not match"
    );

    for (const auto& [key, value] : query_param_map) {
        const auto& req_map = request.GetHttpQueryParameterMap();
        auto found = req_map.find(key);
        if (found == req_map.end()) {
            result += check_and_print_results(
                key,
                "",
                "Query parameters key not found in request map"
            );
        } else {
            result += check_and_print_results(
                value,
                found->second,
                "Query parameters values do not match"
            );
        }
    }

    result += check_and_print_results(
        false,
        request.GetBody().has_value(),
        "Post body should not have a value"
    );

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}
}
