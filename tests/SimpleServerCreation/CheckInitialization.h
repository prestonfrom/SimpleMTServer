#pragma once

#include "utilities/utility_functions.h"
#include "simple_server/SimpleServer.h"
#include "simple_server/HttpRequest/HttpRequestEnums.h"

namespace init_test {
const std::string port_8080 = "8080";
const std::filesystem::path directory("../../tests/test_files");
const std::string bad_request_message = "HTTP/1.1 400 BAD REQUEST\n"
                                        "content-length: 39\r\n\r\n"
                                        "<html><body>404 Not found</body></html>";
const std::string outgoing_message = "GET / HTTP/1.0";
const uint64_t num_of_threads = 5;

uint64_t test_port_number() {
    simple_server::SimpleServer simpleserver(port_8080, directory, num_of_threads);
    auto result = check_and_print_results(
        port_8080,
        simpleserver.GetPortNumber(),
        "Testing port number is correctly set after creation of object -- Port numbers do not match."
    );
    return result;
}

uint64_t test_directory_name() {
    simple_server::SimpleServer simpleserver(port_8080, directory, num_of_threads);
    auto result = check_and_print_results(
        directory,
        simpleserver.GetFileDirectory(),
        "Testing file directory is correctly set after creation of object -- directories do not match."
    );
    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

uint64_t test_number_of_threads() {
    simple_server::SimpleServer simpleserver(port_8080, directory, num_of_threads);
    auto result = check_and_print_results(
        num_of_threads,
        simpleserver.GetNumberOfThreads(),
        "Testing number of threads is correctly set after creation of object -- number of threads does not match."
    );
    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

uint64_t test_create_listener_fd() {
    simple_server::SimpleServer simpleserver(port_8080, directory, num_of_threads);
    simpleserver.Run();

    auto result = check_and_print_socket_results(
        outgoing_message,
        port_8080,
        bad_request_message,
        "Did not receive expected 404 message from Server."
    );

    simpleserver.Stop();
    print_function_name_and_result(__FUNCTION__, result);
    return result;
}
}
