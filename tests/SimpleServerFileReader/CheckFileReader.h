#pragma once

#include "utilities/utility_functions.h"
#include "simple_server/FileReader/FileReader.h"

namespace file_read_test {
const std::string directory = "../../tests/test_files";
const std::string hello_world_file = "hello_world.html";

const std::string file_contents = "<!DOCTYPE html>\n"
                                  "<html lang=\"en\">\n"
                                  "<head>\n"
                                  "    <meta charset=\"UTF-8\">\n"
                                  "    <title>Hello, World!</title>\n"
                                  "</head>\n"
                                  "<body>\n"
                                  "Welcome!<br>\n"
                                  "Everything is fine!\n"
                                  "</body>\n"
                                  "</html>";

const std::chrono::milliseconds refresh_time{100};

uint64_t test_starting_file_reader() {
    simple_server::FileReader fr(directory, refresh_time);

    fr.Run();

    using namespace std::chrono_literals;
    std::this_thread::sleep_for(100ms);

    auto result = check_and_print_results(
        true,
        fr.GetFile(hello_world_file).has_value(),
        "File reader should have hello_world.html"
    );

    fr.Stop();

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

uint64_t test_file_contents() {
    simple_server::FileReader fr(directory, refresh_time);

    fr.Run();

    using namespace std::chrono_literals;
    std::this_thread::sleep_for(100ms);

    size_t result = 0;

    if (auto file_opt = fr.GetFile(hello_world_file); file_opt.has_value()) {
        result += check_and_print_results(
            file_contents,
            file_opt.value().get().GetContents(),
            "File contents do not match expected contents"
        );
    } else {
        result = check_and_print_results(
            true,
            file_opt.has_value(),
            "File reader should have hello_world.html contents"
        );
    }

    fr.Stop();

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}
}
