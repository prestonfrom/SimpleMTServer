#include "SimpleServerCreation/CheckInitialization.h"
#include "SimpleServerFileReader/CheckFileReader.h"
#include "SimpleServerRequestParsing/CheckParsing.h"
#include "SimpleServerListener/CheckListenerThread.h"

#include <csignal>
#include <filesystem>

int main(int argc, char ** argv) {
    signal(SIGPIPE, SIG_IGN);

    if (argc > 1) {
        if (std::string{argv[1]} == "debug") {
            g_debug = true;
        }
    }

    uint64_t results{0};
    try {
        results += init_test::test_port_number();
        results += init_test::test_directory_name();
        results += init_test::test_number_of_threads();
        results += init_test::test_create_listener_fd();

        results += parsing_test::test_http_method_enum();
        results += parsing_test::test_parsing_headers();
        results += parsing_test::test_parsing_request_headers();
        results += parsing_test::test_parsing_post_body();
        results += parsing_test::test_parsing_get_body();

        results += parsing_test::test_parsing_get_query_params();

        results += file_read_test::test_starting_file_reader();
        results += file_read_test::test_file_contents();

        results += listener_tests::test_default_get_request();
        results += listener_tests::test_get_request();
        results += listener_tests::test_get_subdir_file();
    } catch (const std::exception& e) {
        std::cout << "Test caught exception: " << e.what() << "\n";
        results += 1;
    }

    if (results > 0) {
        return static_cast<int>(results);
    } else {
        return 0;
    }
}
