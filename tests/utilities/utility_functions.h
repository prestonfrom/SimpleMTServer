#pragma once

#include "simple_server/HttpRequest/HttpRequestEnums.h"
#include "utilities/print_errors.h"

bool g_debug{false};

auto print_function_name_and_result(std::string function_name, uint64_t result);

template <class ExpectedType, class ReceivedType, class MessageType>
auto check_and_print_results(ExpectedType expected, ReceivedType received, MessageType message) -> uint64_t;


template <class ExpectedType, class ReceivedType, class MessageType>
auto check_and_print_socket_results(
    std::string outgoing_message,
    const std::string& port,
    ExpectedType expected,
    ReceivedType received,
    MessageType message
) -> uint64_t;

#include "utility_functions.tcc"
