#include <iostream>

/*
 * Helper struct that defines value to be true_type when T has an ostream << operator.
 */
template <typename T>
struct has_ostream_operator{
    /*
     * If type U has an ostream << operator, then this function is available and the return type of
     * test is std::true_type. decltype only takes one argument, but it can take an expression, which
     * is created by the comma used here, and the return type is the type of the item after the comma.
     * Thanks to SFINAE, this won't be available if U does not have an ostream operator, in which case
     * we'd fall back to test(...), which returns std::false_type.
     */
    template <typename U>
    static auto test(U) -> decltype(std::cout << std::declval<U>(), std::true_type{});
    static auto test(...) -> std::false_type;

    /*
     * This allows us to call can_print<T>::value, which will either be true if the ostream << operator
     * is available and false othewise.
     */
    using value = decltype(test(std::declval<T>()));
};

/*
 * Use can_print to get value (true_type/false_type) from has_ostream_operator.
 */
template<typename T>
struct can_print : public has_ostream_operator<T>::value {};

// If it has the ostream operator, stream it.
template <typename T, typename std::enable_if<can_print<T>::value, int>::type = 0>
auto print(std::ostream& stream, T& t) -> std::ostream& {
    stream << t;
    return stream;
}

// If it doesn't, call to string, because we expect it to be an enum we have a to_string fuction for.
template <typename T, typename std::enable_if<!can_print<T>::value, int>::type = 0>
auto print(std::ostream& stream, T& t) -> std::ostream& {
    stream << simple_server::to_string(t);
    return stream;
}

// For bools, print "true" or "false"
template <>
auto print(std::ostream& stream, bool& t) -> std::ostream& {
    if (t) {
        stream << "true";
    } else {
        stream << "false";
    }
    return stream;
}

// Call to print error messages
template <typename Expected, typename Received>
void print_error_message(Expected expected, Received received, std::string_view message)
{
    std::cout << "\033[0;31m" << message << "\n";
    std::cout << "Expected:\n";
    print(std::cout, expected);
    std::cout << "\nbut received:\n";
    print(std::cout, received);
    std::cout << "\n\033[0m\n";
}

