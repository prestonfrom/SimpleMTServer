#include "utilities/utility_functions.h"

#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <arpa/inet.h>
#include <iostream>
#include <thread>

auto print_function_name_and_result(std::string function_name, uint64_t result) {
    if (g_debug) {
        std::cout << function_name << " had " << result << " failure(s).\n\n";
    }
}

template <class ExpectedType, class ReceivedType, class MessageType>
auto check_and_print_results(ExpectedType expected, ReceivedType received, MessageType message) -> uint64_t {
    if (expected == received) {
        return 0;
    } else {
        print_error_message(expected, received, message);
        return 1;
    }
}

template <class ExpectedType, class MessageType>
auto check_and_print_socket_results(
    std::string outgoing_message,
    const std::string& port,
    ExpectedType expected,
    MessageType message
) -> uint64_t {
    /** Pretty much copy-and-pasted from Beej's Guide for expedience. **/
    int sockfd, numbytes;
    sockfd = 0;
    char buf[1000];
    struct addrinfo hints, *servinfo, *p;
    int rv;

    // Set socket options: Timeout after 1 second, reuse address and reuse port.
    int yes = 1;
    struct timeval tv;
    tv.tv_sec = 1;
    tv.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof tv);
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof tv);

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    if ((rv = getaddrinfo("localhost", port.c_str(), &hints, &servinfo)) != 0) {
        std::cout << message << "\n";
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
        return 1;
    }

    // loop through all the results and connect to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((sockfd = socket(p->ai_family, p->ai_socktype,
                             p->ai_protocol)) == -1) {
            perror("client: socket");
            continue;
        }

        if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            perror("client: connect");
            printf("Trying next...\n");
            continue;
        }

        break;
    }

    if (p == NULL) {
        std::cout << message << "\n";
        std::cout << "client: failed to connect\n";
        close(sockfd);
        return 1;
    }

    ssize_t sent_bytes = 0;
    while (sent_bytes < static_cast<ssize_t>(outgoing_message.length()) && sent_bytes != -1) {
        sent_bytes = send(sockfd, outgoing_message.data(), outgoing_message.length(), 0);
    }

    freeaddrinfo(servinfo); // all done with this structure

    size_t count = 0;
    while (count < 10) {
        numbytes = recv(sockfd, buf, 1000 - 1, 0);
        if (numbytes == -1) {
            perror("recv");
            ++count;
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(100ms);
            //return 1;
        } else {
            break;
        }
    }

    buf[numbytes] = '\0';

    close(sockfd);

    auto received = std::string(buf);
    if (received == expected) {
        return 0;
    } else {
        print_error_message(expected, received, message);
        return 1;
    }
}
