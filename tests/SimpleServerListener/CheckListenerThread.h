#pragma once

#include "utilities/utility_functions.h"
#include "simple_server/SimpleServer.h"

#include <iostream>

namespace listener_tests {
const std::string port_8080 = "8080";
const std::string directory = "../../tests/test_files";
const uint64_t num_of_threads = 5;
const std::string bad_request_message = "HTTP/1.1 400 BAD REQUEST\n"
                                        "content-length: 39\r\n\r\n"
                                        "<html><body>404 Not found</body></html>";
const std::string outgoing_message = "GET / HTTP/1.0";

const std::string hello_world_req = "GET /hello_world.html HTTP/1.0";
const std::string file_contents = "HTTP/1.1 200 OK\n"
                                  "content-length: 163\r\n\r\n"
                                  "<!DOCTYPE html>\n"
                                  "<html lang=\"en\">\n"
                                  "<head>\n"
                                  "    <meta charset=\"UTF-8\">\n"
                                  "    <title>Hello, World!</title>\n"
                                  "</head>\n"
                                  "<body>\n"
                                  "Welcome!<br>\n"
                                  "Everything is fine!\n"
                                  "</body>\n"
                                  "</html>";

const std::string subdir_get_req  = "GET /subdirectory/subfile.html HTTP/1.0";
const std::string subdir_contents = "HTTP/1.1 200 OK\n"
                                    "content-length: 165\r\n\r\n"
                                    "<!DOCTYPE html>\n"
                                    "<html lang=\"en\">\n"
                                    "<head>\n"
                                    "    <meta charset=\"UTF-8\">\n"
                                    "    <title>Subdirectory File</title>\n"
                                    "</head>\n"
                                    "<body>\n"
                                    "This is the subdirectory file.\n"
                                    "</body>\n"
                                    "</html>";


uint64_t test_default_get_request() {
    simple_server::SimpleServer simpleserver(port_8080, directory, num_of_threads);
    simpleserver.Run();

    auto result = check_and_print_socket_results(
        outgoing_message,
        port_8080,
        bad_request_message,
        "Did not receive expected 404 message from Server."
    );

    simpleserver.Stop();

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

uint64_t test_get_request() {
    simple_server::SimpleServer simpleserver(port_8080, directory, num_of_threads);
    simpleserver.Run();

    using namespace std::chrono_literals;
    std::this_thread::sleep_for(100ms);

    auto result = check_and_print_socket_results(
        hello_world_req,
        port_8080,
        file_contents,
        "Did not receive expected hello_world.html file contents from server."
    );

    simpleserver.Stop();

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

uint64_t test_get_subdir_file() {
    simple_server::SimpleServer simpleserver(port_8080, directory, num_of_threads);
    simpleserver.Run();

    using namespace std::chrono_literals;
    std::this_thread::sleep_for(100ms);

    auto result = check_and_print_socket_results(
        subdir_get_req,
        port_8080,
        subdir_contents,
        "Did not receive expected subdirectory file contents from server."
    );

    simpleserver.Stop();

    print_function_name_and_result(__FUNCTION__, result);
    return result;
}

}
