# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/prestonfrom/Projects/CardGame/SimpleMTServer/tests/simple_server_tests.cpp" "/home/prestonfrom/Projects/CardGame/SimpleMTServer/tests/CMakeFiles/simple_server_tests.dir/simple_server_tests.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "tests/PRIVATE"
  "SimpleServerLib/inc"
  "tests"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/prestonfrom/Projects/CardGame/SimpleMTServer/SimpleServerLib/CMakeFiles/SimpleServer.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
