#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NO_COLOR='\033[0m'

BUILD=0
CLEAN=0
EXAMPLE=0
VERBOSE=0
TEST=0
HELP=1
DEBUG=0
PRINT=0
SETUP_CMAKE=0

while getopts ":sbcetvdp" opt; do
    case ${opt} in
        b )
            HELP=0
            BUILD=1
            ;;
        s )
            HELP=0
            SETUP_CMAKE=1
            ;;
        c )
            HELP=0
            CLEAN=1
            ;;
        e )
            HELP=0
            EXAMPLE=1
            ;;
        t )
            HELP=0
            TEST=1
            ;;
        v )
            HELP=0
            VERBOSE=1
            ;;
        d )
            HELP=0
            DEBUG=1
            ;;
        p )
            HELP=0
            PRINT=1
            ;;
    esac
done

if [[ ${HELP} -eq 1 ]]; then
    echo "Usage: "
    echo "  -s to set-up cmake (This will need to be run first. You may need to install CMake.)"
    echo "  -b to build"
    echo "  -c to clean"
    echo "  -e to run example application"
    echo "  -t to run tests"
    echo "  -v to run cmake in verbose mode"
    echo "  -d to run either test or example in GDB"
    echo "  -p to run test with printing on (prints test name before run)"
    echo "Flags can be combined like: "
    echo "  ./simple_server.sh -bcetv"
    exit
fi

if [[ ${SETUP_CMAKE} == 1 ]]; then
    echo "Setting up CMake project."
    cd build && cmake ../ && cd ..
fi

if [[ ${CLEAN} -eq 1 ]]; then
    echo "Make clean and then building..."
    cd build && make clean && cd ..
fi

if [[ ${BUILD} -eq 1 ]]; then

    echo "Building..."
    if [[ ${VERBOSE} -eq 1 ]]; then
        cd build && make VERBOSE=1
    else
        cd build && make
    fi

    if [[ $? -ne 0 ]]; then
        echo -e "${RED}BUILD FAILED.${NO_COLOR}"
        exit
    fi
    cd ..
fi

if [[ ${DEBUG} -eq 1 ]]; then
    if [[ ${TEST} -eq 1 ]]; then
        if [[ ${PRINT} -eq 1 ]]; then
            cd bin/tests && gdb -ex run ./simple_server_tests debug
        else
            cd bin/tests && gdb -ex run ./simple_server_tests
        fi
    fi
    if [[ ${EXAMPLE} -eq 1 ]]; then
        cd bin && gdb -ex run --args ./create_simple_server 8080 "../tests/test_files" 4
        cd ..
    fi
else
    if [[ ${TEST} -eq 1 ]]; then
        printf "\n****** Running tests ******\n"
        if [[ ${PRINT} -eq 1 ]]; then
            cd bin/tests && ./simple_server_tests debug
        else
            cd bin/tests && ./simple_server_tests
        fi
        results=$?

        if [[ ${results} -ne 0 ]];
        then
            printf "\n****** Test results  ******\n"
            echo -e "${RED}THERE WERE $results FAILURES IN TESTS.${NO_COLOR}"
        else
            printf "****** Test results  ******\n"
            echo -e "${GREEN}TESTS SUCCESSFUL.${NO_COLOR}"
        fi
    fi

    if [[ ${EXAMPLE} -eq 1 ]]; then
        cd bin && ./create_simple_server 8080 "../tests/test_files" 4
        cd ..
    fi
fi
