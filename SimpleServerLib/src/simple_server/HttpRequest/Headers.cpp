#include "simple_server/HttpRequest/Headers.h"

namespace simple_server {

auto Headers::AddHeader(std::string header, std::string content) -> void {
    m_headers.emplace(std::move(header), std::move(content));
}

auto Headers::AddHeaderFromString(std::string_view header_to_parse) -> void {
    auto split_pos = header_to_parse.find(':');
    std::string_view name = header_to_parse.substr(0, split_pos);
    std::string_view content = header_to_parse.substr(split_pos + 1);

    while (content.front() == ' ') {
        content.remove_prefix(1);
    }

    AddHeader(std::string(name), std::string(content));
}

auto Headers::GetHeaders() -> const std::unordered_map<std::string, std::string>& {
    return m_headers;
}

auto Headers::GetHeaders() const -> const std::unordered_map<std::string, std::string>& {
    return m_headers;
}

}
