#include "simple_server/HttpRequest/HttpRequestEnums.h"

namespace simple_server {

const std::string METHOD_GET = "GET";
const std::string METHOD_HEAD = "HEAD";
const std::string METHOD_POST = "POST";
const std::string METHOD_PUT = "PUT";
const std::string METHOD_DELETE = "DELETE";
const std::string METHOD_CONNECT = "CONNECT";
const std::string METHOD_OPTIONS = "OPTIONS";
const std::string METHOD_TRACE = "TRACE";
const std::string METHOD_PATCH = "PATCH";
const std::string METHOD_INVALID = "INVALID";

template<>
auto to_enum<HttpMethod>(std::string_view method_string) -> HttpMethod {
	if (method_string.length() < 3) {
		return HttpMethod::INVALID;
	}

	if (method_string == METHOD_GET) {
		return HttpMethod::GET;
	} else if (method_string == METHOD_HEAD) {
		return HttpMethod::HEAD;
	} else if (method_string == METHOD_POST) {
		return HttpMethod::POST;
	} else if (method_string == METHOD_PUT) {
		return HttpMethod::PUT;
	} else if (method_string == METHOD_DELETE) {
		return HttpMethod::DELETE;
	} else if (method_string == METHOD_CONNECT) {
		return HttpMethod::CONNECT;
	} else if (method_string == METHOD_OPTIONS) {
		return HttpMethod::OPTIONS;
	} else if (method_string == METHOD_TRACE) {
		return HttpMethod::TRACE;
	} else if (method_string == METHOD_PATCH) {
		return HttpMethod::PATCH;
	}

	return HttpMethod::INVALID;
}

auto to_string(HttpMethod http_method) -> const std::string& {
	switch (http_method) {
		case HttpMethod::GET:
			return METHOD_GET;
		case HttpMethod::HEAD:
			return METHOD_HEAD;
		case HttpMethod::POST:
			return METHOD_POST;
		case HttpMethod::PUT:
			return METHOD_PUT;
		case HttpMethod::DELETE:
			return METHOD_DELETE;
		case HttpMethod::CONNECT:
			return METHOD_CONNECT;
		case HttpMethod::OPTIONS:
			return METHOD_OPTIONS;
		case HttpMethod::TRACE:
			return METHOD_TRACE;
		case HttpMethod::PATCH:
			return METHOD_PATCH;
		case HttpMethod::INVALID:
		default:
			return METHOD_INVALID;
	}
}

const std::string HTTP_VERSION_INVALID = "INVALID";
const std::string HTTP_VERSION_0_9 = "HTTP/0.9";
const std::string HTTP_VERSION_1_0 = "HTTP/1.0";
const std::string HTTP_VERSION_1_1 = "HTTP/1.1";
const std::string HTTP_VERSION_2_0 = "HTTP/2.0";

template<>
auto to_enum<HttpVersion>(std::string_view method_string) -> HttpVersion {
	if (method_string == HTTP_VERSION_0_9) {
		return HttpVersion::v0_9;
	} else if (method_string == HTTP_VERSION_1_0) {
		return HttpVersion::v1_0;
	} else if (method_string == HTTP_VERSION_1_1) {
		return HttpVersion::v1_1;
	} else if (method_string == HTTP_VERSION_2_0) {
		return HttpVersion::v2_0;
	}
	return HttpVersion::INVALID;
}

auto to_string(HttpVersion http_version) -> const std::string& {
	switch (http_version) {
		case HttpVersion::INVALID:
			return HTTP_VERSION_INVALID;
		case HttpVersion::v0_9:
			return HTTP_VERSION_0_9;
		case HttpVersion::v1_0:
			return HTTP_VERSION_1_0;
		case HttpVersion::v1_1:
			return HTTP_VERSION_1_1;
		case HttpVersion::v2_0:
		default:
			return HTTP_VERSION_2_0;
	}
}
}
