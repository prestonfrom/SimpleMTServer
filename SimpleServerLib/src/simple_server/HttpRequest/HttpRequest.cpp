#include "simple_server/HttpRequest/HttpRequest.h"

#include <vector>

namespace simple_server {

auto HttpRequest::ParseIncomingMessage(std::string_view incoming_message) -> void {
    auto start_pos = extractRequestLine(incoming_message);
    start_pos = extractHeaders(incoming_message, start_pos);

    // If the query parameters are long enough, they'll be moved to the body,
    // so always extract the body even if it's not a POST, etc. request.
    extractBody(incoming_message, start_pos);
}

auto HttpRequest::GetHttpMethod() const -> HttpMethod {
    return m_http_method;
}

auto HttpRequest::GetHttpPath() const -> const std::string& {
    return m_http_path;
}

auto HttpRequest::GetHttpQueryParameterMap() const -> const ParamMap& {
    return m_parameter_map;
}

auto HttpRequest::GetHttpQueryParameterString() const -> const std::string& {
    return m_parameter_string;
}

auto HttpRequest::GetHttpVersion() const -> HttpVersion {
    return m_http_version;
}

auto HttpRequest::GetHeaders() -> const Headers& {
    return m_incoming_headers;
}

auto HttpRequest::GetHeaders() const -> const Headers& {
    return m_incoming_headers;
}

auto HttpRequest::GetBody() const -> const std::optional<std::string>& {
    return m_incoming_post_body;
}

auto HttpRequest::extractRequestLine(std::string_view message) -> size_t {
    auto end_pos = message.find(' ');
    m_http_method = to_enum<HttpMethod>(message.substr(0, end_pos));

    auto start_pos = end_pos + 1;
    end_pos = message.find(' ', start_pos);
    extractPathAndQueryParam(message.substr(start_pos, end_pos - start_pos));

    start_pos = end_pos + 1;
    end_pos = message.find('\n', start_pos);
    m_http_version = to_enum<HttpVersion>(message.substr(start_pos, end_pos - start_pos));

    return end_pos + 1;
}

auto HttpRequest::extractPathAndQueryParam(std::string_view path_and_params) -> void {
    if (path_and_params[0] == '/') {
        path_and_params.remove_prefix(1);
    }

    auto question_mark = path_and_params.find('?');
    m_http_path = path_and_params.substr(0, question_mark);
    if (question_mark != std::string_view::npos) {
        m_parameter_string = path_and_params.substr(question_mark + 1);
        extractQueryParamMap(m_parameter_string);
    }
}

auto HttpRequest::extractQueryParamMap(std::string_view query_params) -> void {
    size_t split = query_params.find('=');
    size_t stop  = query_params.find('&');

    while (split != std::string_view::npos) {
        m_parameter_map.emplace(std::string{query_params.substr(0, split)},
                                std::string{query_params.substr(split + 1, (stop - split - 1))});

        if (stop == std::string_view::npos) {
            break;
        }

        query_params.remove_prefix(stop + 1);

        split = query_params.find('=');
        stop = query_params.find('&');
    }
}

static auto find_next_end(std::string_view message, size_t header_start, size_t& header_end) -> size_t {
    auto rn = message.find("\r\n", header_start);
    auto n  = message.find("\n", header_start);
    auto r  = message.find("\r", header_start);

    if (rn < n && rn < r) {
        header_end = rn;
        return 2;
    } else {
        header_end = std::min(r, n);
        return 1;
    }
}

auto HttpRequest::extractHeaders(std::string_view message, size_t start_pos) -> size_t {
    auto end_pos = message.find("\r\n\r\n", start_pos);
    if (end_pos == std::string_view::npos) {
        return end_pos;
    }
    size_t header_start = start_pos;
    size_t header_end = start_pos;
    size_t end_size = 0;

    while (header_start < end_pos) {
        end_size = find_next_end(message, header_start, header_end);

        m_incoming_headers.AddHeaderFromString(message.substr(header_start, header_end - header_start));
        header_start = header_end + end_size;
    }

    return end_pos;
}

auto HttpRequest::extractBody(std::string_view message, size_t start_pos) -> void {
    if (start_pos != std::string_view::npos) {
        // Add 4 in order to skip the \r\n\r\n between headers and body.
        start_pos += 4;
        if (start_pos < message.length()) {
            m_incoming_post_body = std::string{message.substr(start_pos)};
        }
    }
}

}
