#include "simple_server/SocketQueue/SocketQueueCollection.h"

namespace simple_server {
SocketQueueCollection::SocketQueueCollection(size_t size) {
    m_collection.reserve(size);
    for (size_t queue = 0; queue < size; ++queue) {
        m_collection.emplace_back(queue, 100);
    }
}

auto SocketQueueCollection::AddSocket(int socket_fd) -> void {
    auto& queue = m_collection[m_current_queue++];
    queue.Add(socket_fd);
    if (m_current_queue == m_collection.size()) {
        m_current_queue = 0;
    }
}

auto SocketQueueCollection::GetQueue(size_t index) -> simple_server::AtomicSocketQueue& {
    return m_collection[index];
}

auto SocketQueueCollection::Stop() -> void {
    for (auto& queue : m_collection) {
        queue.Stop();
    }
}

}

