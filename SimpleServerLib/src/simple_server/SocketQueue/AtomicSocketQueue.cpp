#include "simple_server/SocketQueue/AtomicSocketQueue.h"

namespace simple_server {

AtomicSocketQueue::AtomicSocketQueue(size_t id, size_t max_size)
    : m_id(id),
      m_max_size(max_size) {
    m_queue.reserve(m_max_size);
}

AtomicSocketQueue::AtomicSocketQueue(AtomicSocketQueue&& rhs) {
    std::lock_guard<std::mutex> g{rhs.m_lock};
    std::lock_guard<std::mutex> l{m_lock};
    m_head = rhs.m_head;
    m_tail = rhs.m_tail;
    m_queue = std::move(rhs.m_queue);
    m_max_size = rhs.m_max_size;
}

auto AtomicSocketQueue::Add(int socket_fd) -> void {
    std::lock_guard<std::mutex> g{m_lock};
    m_queue[m_tail++] = socket_fd;
    auto size = m_queue.size();
    if (size == m_max_size) {
        resize();
    } else if (m_tail > m_max_size) {
        m_tail = 0;
    }

    m_condition_variable.notify_all();
}

auto AtomicSocketQueue::Pop() -> int {
    std::lock_guard<std::mutex> g{m_lock};
    int result = m_queue[m_head];
    m_head++;
    if (m_head > m_max_size) {
        m_head = 0;
    }
    return result;
}

auto AtomicSocketQueue::HasItem() -> bool {
    return m_head != m_tail;
}

auto AtomicSocketQueue::resize() -> void {
    std::lock_guard<std::mutex> g{m_lock};
    m_max_size *= 2;
    std::vector<int> temp;
    temp.reserve(m_max_size);
    size_t temp_pos = 0;
    for (size_t pos = m_head; pos < m_queue.size(); ++pos) {
        temp[temp_pos++] = m_queue[pos];
    }
    for (size_t pos = 0; pos < m_tail; ++pos) {
        temp[temp_pos++] = m_queue[pos];
    }
    m_head = 0;
    m_tail = temp_pos;
    m_queue.swap(temp);
}

auto AtomicSocketQueue::GetConditionVariable() -> std::condition_variable& {
    return m_condition_variable;
}


auto AtomicSocketQueue::Stop() -> void {
    m_condition_variable.notify_all();
}

}
