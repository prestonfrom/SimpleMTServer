#include "simple_server/SimpleServer.h"

#include <iostream>
#include <netdb.h>
#include <cstring>
#include <unistd.h>
#include <sys/epoll.h>

namespace simple_server {

using namespace std::chrono_literals;

SimpleServer::SimpleServer(
    std::string port_number,
    std::filesystem::path file_directory,
    uint64_t number_of_threads
) : m_port_number(std::move(port_number)),
    m_file_directory(std::move(file_directory)),
    m_number_of_threads(number_of_threads),
    m_file_reader(m_file_directory, 100ms),
    m_socket_queue_collection{number_of_threads} {}

auto SimpleServer::GetPortNumber() const -> const std::string& {
    return m_port_number;
}

auto SimpleServer::GetFileDirectory() const -> const std::filesystem::path& {
    return m_file_directory;
}

auto SimpleServer::GetNumberOfThreads() const -> uint64_t {
    return m_number_of_threads;
}

auto SimpleServer::GetFileReader() const -> const FileReader& {
    return m_file_reader;
}

auto SimpleServer::Run() -> void {
    m_listener_thread = std::thread(&SimpleServer::listener, this);
    for (size_t i = 0; i < m_number_of_threads; ++i) {
        m_worker_threads.emplace_back(std::thread(&SimpleServer::worker, this, i));
    }
    m_file_reader.Run();
}

auto SimpleServer::Stop() -> void {
    m_run = false;
    m_file_reader.Stop();
    m_listener_thread.join();
    m_socket_queue_collection.Stop();
    for (auto& thread : m_worker_threads) {
        thread.join();
    }
}

auto SimpleServer::AddApiCallback(std::string path, std::function<std::string(HttpRequest)> callback) -> void {
    m_api_paths.emplace(std::move(path), std::move(callback));
}

static auto can_read_and_write_socket(uint32_t event) -> bool {
    /*printf("EPOLLIN        : %i\n", event & EPOLLIN);
    printf("EPOLLPRI       : %i\n", event & EPOLLPRI);
    printf("EPOLLOUT       : %i\n", event & EPOLLOUT);
    printf("EPOLLRDNORM    : %i\n", event & EPOLLRDNORM);
    printf("EPOLLRDBAND    : %i\n", event & EPOLLRDBAND);
    printf("EPOLLWRNORM    : %i\n", event & EPOLLWRNORM);
    printf("EPOLLWRBAND    : %i\n", event & EPOLLWRBAND);
    printf("EPOLLMSG       : %i\n", event & EPOLLMSG);
    printf("EPOLLERR       : %i\n", event & EPOLLERR);
    printf("EPOLLHUP       : %i\n", event & EPOLLHUP);
    printf("EPOLLRDHUP     : %i\n", event & EPOLLRDHUP);
    printf("EPOLLEXCLUSIVE : %i\n", event & EPOLLEXCLUSIVE);
    printf("EPOLLWAKEUP    : %i\n", event & EPOLLWAKEUP);
    printf("EPOLLONESHOT   : %i\n", event & EPOLLONESHOT);
    printf("EPOLLET        : %i\n\n", event & EPOLLET);*/

    if ((event & EPOLLIN) == 1 && (event & EPOLLERR) == 0 && (event & EPOLLHUP) == 0 && (event & EPOLLRDHUP) == 0) {
        return true;
    }

    return false;
}

auto SimpleServer::listener() -> void {
    sockaddr_storage incoming_address;
    socklen_t socket_length;

    auto listen_socket = createListenFD();

    /** Based on epoll man page! http://man7.org/linux/man-pages/man7/epoll.7.html */
    const int MAX_EVENTS = 10;
    epoll_event ev, events[MAX_EVENTS];

    ssize_t nfds = 0;
    int epollfd = 0;

    epollfd = epoll_create1(0);
    if (epollfd == -1) {
        perror("epoll_create1");
        exit(EXIT_FAILURE);
    }

    ev.events = EPOLLIN;
    ev.data.fd = listen_socket;
    if (epoll_ctl(epollfd, EPOLL_CTL_ADD, listen_socket, &ev) == -1) {
        perror("epoll_ctl: listen_sock");
        exit(EXIT_FAILURE);
    }

    while (m_run) {
        nfds = epoll_wait(epollfd, events, MAX_EVENTS, 1);
        if (nfds == -1) {
            perror("epoll_wait");
            exit(EXIT_FAILURE);
        }

        for (ssize_t n = 0; n < nfds; ++n) {
            if (events[n].data.fd == listen_socket) {
                auto conn_sock = accept(
                    listen_socket,
                    (struct sockaddr*) &incoming_address,
                    &socket_length
                );

                if (conn_sock == -1) {
                    perror("accept");
                    shutdown(conn_sock, SHUT_RDWR);
                }
                else {
                    ev.events = EPOLLIN | EPOLLET; // | EPOLLEXCLUSIVE;
                    ev.data.fd = conn_sock;
                    if (epoll_ctl(epollfd, EPOLL_CTL_ADD, conn_sock, &ev) == -1) {
                        perror("epoll_ctl: conn_sock");
                    }
                }
            } else {
                if (can_read_and_write_socket(events[n].events)) {
                    m_socket_queue_collection.AddSocket(events[n].data.fd);
                } else {
                    if (epoll_ctl(epollfd, EPOLL_CTL_DEL, events[n].data.fd, nullptr) < 0) {
                        perror("epoll_ctl:");
                    }

                    close(events[n].data.fd);
                }
            }
        }
    }

    close(listen_socket);
}

auto SimpleServer::worker(size_t id) -> void {
    auto& queue = m_socket_queue_collection.GetQueue(id);
    auto& cv = queue.GetConditionVariable();
    std::mutex mtx;
    while (m_run) {
        if (queue.HasItem()) {
            auto socket_fd = queue.Pop();
            ssize_t read_bytes = 0;

            char char_buffer[1000];
            std::string buffer;
            bool error{false};
            do {
                read_bytes = recv(socket_fd, char_buffer, 1000, 0);
                if (read_bytes >= 0) {
                    buffer.append(char_buffer, static_cast<unsigned long>(read_bytes));
                } else {
                    error = true;
                    break;
                }
            } while (read_bytes == 1000);

            if (!error) {
                std::string response;

                if (!buffer.empty()) {
                    HttpRequest request;
                    request.ParseIncomingMessage(buffer);

                    response.reserve(5108);
                    if (auto& file = m_file_reader.GetFile(request.GetHttpPath()); file.has_value()) {
                        const std::string& contents = file.value().get().GetContents();
                        response = "HTTP/1.1 200 OK\n";
                        response.append("content-length: ");
                        response.append(std::to_string(contents.length()));
                        response.append("\r\n\r\n").append(contents);
                    } else if (auto found = m_api_paths.find(request.GetHttpPath()); found != m_api_paths.end()) {
                        auto contents = found->second(std::move(request));
                        response = "HTTP/1.1 200 OK\n";
                        response.append("content-length: ");
                        response.append(std::to_string(contents.length()));
                        response.append("\r\n\r\n").append(contents);
                    }
                }

                if (response.empty()) {
                    response = m_file_reader.Get404();
                }

                ssize_t sent_bytes = 0;
                while (sent_bytes < static_cast<ssize_t>(response.length()) && sent_bytes != -1) {
                    sent_bytes = send(socket_fd, response.data(), response.length(), 0);
                }
            } else {
                close(socket_fd);
            }
        } else {
            using namespace std::chrono_literals;
            std::unique_lock<std::mutex> lock(mtx);
            // Sleep for 1 second, but we'll wake up if a stop signal is received or if a new item is in the queue.
            cv.wait_for(lock, 1000ms);
        }
    }
}


auto SimpleServer::createListenFD() -> int {
    int listen_socket;
    addrinfo hints;
    addrinfo* server_info;
    memset(&hints, 0, sizeof hints);

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    auto failure = getaddrinfo(NULL, m_port_number.c_str(), &hints, &server_info);
    if (failure) {
        throw std::runtime_error("Could not get addr info");
    }

    while ((server_info = server_info->ai_next)) {
        listen_socket = socket(
            server_info->ai_family,
            server_info->ai_socktype,
            server_info->ai_protocol
        );

        if (listen_socket == -1) {
            throw std::runtime_error("Could not get create socket");
        } else {
            int yes = 1;
            failure = setsockopt(
                listen_socket,
                SOL_SOCKET,
                SO_REUSEADDR,
                &yes,
                sizeof(int)
            );
            if (failure == -1) {
                throw std::runtime_error("Could not set socket to reuse address");
            }

            failure = setsockopt(
                listen_socket,
                SOL_SOCKET,
                SO_REUSEPORT,
                &yes,
                sizeof(int)
            );
            if (failure == -1) {
                throw std::runtime_error("Could not set socket to reuse port");
            }
        }

        failure = bind(listen_socket, server_info->ai_addr, server_info->ai_addrlen);
        if (failure == -1) {
            perror("Could not bind listen socket");
            close(listen_socket);
            throw std::runtime_error("Could not bind listen socket");
        }
    }

    freeaddrinfo(server_info);

    failure = listen(listen_socket, 32);

    if (failure == -1) {
        close(listen_socket);
        throw std::runtime_error("Could not start listening on listen socket");
    }

    return listen_socket;
}

}
