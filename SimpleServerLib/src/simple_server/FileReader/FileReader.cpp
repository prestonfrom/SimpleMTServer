#include "simple_server/FileReader/FileReader.h"

#include <iostream>
#include <fstream>

namespace simple_server {
FileReader::FileReader(std::filesystem::path file_directory, std::chrono::milliseconds refresh_time)
    : m_path(std::move(file_directory)),
      m_refresh_time(refresh_time) {}

auto FileReader::GetFile(const std::string& file_name) const -> const std::optional<std::reference_wrapper<const File>> {
    auto found = m_file_map.find(file_name);
    if (found != m_file_map.end()) {
        return {std::cref(found->second)};
    }
    return {};
}

auto FileReader::Run() -> void {
    m_refresh_thread = std::thread(&FileReader::checkDirectory, this);
}

auto FileReader::Stop() -> void {
    if (m_run) {
        m_run.store(false);
    }

    m_refresh_thread.join();
}

auto FileReader::ListFiles() const -> void {
    if (m_file_map.empty()) {
        std::cout << "File Reader has no files!\n";
        return;
    }

    for (auto&[name, file] : m_file_map) {
        std::cout << name << "\n";
    }
}

auto FileReader::Get404() const -> std::string {
    std::string response;
    std::string contents = "<html><body>404 Not found</body></html>";
    response = "HTTP/1.1 400 BAD REQUEST\n";
    response.append("content-length: ");
    response.append(std::to_string(contents.length()));
    response.append("\r\n\r\n").append(contents);
    return response;
}

static auto set_file(std::filesystem::directory_entry& entry, File& file) {
    file.SetLastFileWrite(entry.last_write_time());

    std::ifstream file_stream(entry.path().string(), std::ifstream::binary);
    if (file_stream.is_open()) {
        std::string buffer(entry.file_size(), '\0');
        file_stream.read(buffer.data(), entry.file_size());
        file.SetContents(std::move(buffer));
    }

}

auto FileReader::checkSubdirectory(std::filesystem::directory_entry& entry, std::string directory_name) -> void {
    for (auto f : std::filesystem::directory_iterator(entry)) {
        auto name = directory_name.append("/").append(f.path().filename().string());
        if (f.is_directory()) {
            checkSubdirectory(f, std::move(name));
        } else {
            auto file = m_file_map.emplace(std::move(name), File{});
            set_file(f, file.first->second);
        }
    }
}

auto FileReader::checkDirectory() -> void {
    while (m_run) {
        for (auto f : std::filesystem::directory_iterator(m_path)) {
            auto name = f.path().filename().string();

            auto found = m_file_map.find(name);
            if (found != m_file_map.end()) {
                if (found->second.GetLastFileWrite() != f.last_write_time()) {
                    set_file(f, found->second);
                }
            } else {
                if (f.is_directory()) {
                    checkSubdirectory(f, std::move(name));
                } else {
                    auto file = m_file_map.emplace(std::move(name), File{});
                    set_file(f, file.first->second);
                }
            }
        }
        std::this_thread::sleep_for(m_refresh_time);
    }
}
}
