#include "simple_server/FileReader/File.h"

namespace simple_server {

auto File::GetLastFileWrite() -> file_clock {
    return m_last_file_write;
}

auto File::SetLastFileWrite(file_clock last_file_write) -> void {
    m_last_file_write = last_file_write;
}

auto File::GetContents() const -> const std::string& {
    return m_file_contents;
}

auto File::SetContents(std::string contents) -> void {
    m_file_contents = std::move(contents);
}
}
