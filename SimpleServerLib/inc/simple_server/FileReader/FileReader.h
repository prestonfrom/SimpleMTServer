#pragma once

#include "simple_server/FileReader/File.h"

#include <atomic>
#include <filesystem>
#include <chrono>
#include <string>
#include <thread>
#include <unordered_map>

namespace simple_server {

class FileReader {
public:
    FileReader(std::filesystem::path file_directory, std::chrono::milliseconds refresh_time);

    FileReader(const FileReader&) = delete;

    FileReader(FileReader&&) = default;

    ~FileReader() = default;

    auto GetFile(const std::string& file_name) const -> const std::optional<std::reference_wrapper<const File>>;

    auto Run() -> void;

    auto Stop() -> void;

    auto ListFiles() const -> void;

    auto Get404() const -> std::string;

private:
    auto checkSubdirectory(std::filesystem::directory_entry& entry, std::string directory_name) -> void;
    auto checkDirectory() -> void;

    std::atomic_bool m_run{true};
    std::filesystem::path m_path;
    std::chrono::milliseconds m_refresh_time;
    std::unordered_map<std::string, File> m_file_map;
    std::thread m_refresh_thread;
};

}
