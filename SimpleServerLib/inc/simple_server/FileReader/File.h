#pragma once

#include <chrono>
#include <string>
#include <filesystem>

namespace simple_server {
using file_clock = std::filesystem::file_time_type;
class File {
public:
    auto GetLastFileWrite() -> file_clock;

    auto SetLastFileWrite(file_clock last_file_write) -> void;

    auto GetContents() const -> const std::string&;

    auto SetContents(std::string contents) -> void;

private:
    file_clock m_last_file_write;
    std::string m_file_contents;

};
}
