#pragma once

#include "simple_server/HttpRequest/HttpRequest.h"
#include "simple_server/FileReader/FileReader.h"
#include "simple_server/SocketQueue/SocketQueueCollection.h"

#include <atomic>
#include <string>
#include <thread>
#include <optional>
#include <functional>

namespace simple_server {

class SimpleServer {
public:
    SimpleServer(
        std::string port_number,     /// Need to know what port to bind to
        std::filesystem::path file_directory,  /// Need f
        uint64_t number_of_threads
    );

    /// Should not be moving the server around.
    SimpleServer(const SimpleServer&) = delete;
    SimpleServer(SimpleServer&&) = delete;

    ~SimpleServer() = default;

    auto GetPortNumber() const -> const std::string&;

    auto GetFileDirectory() const -> const std::filesystem::path&;

    auto GetNumberOfThreads() const -> uint64_t;

    auto GetFileReader() const -> const FileReader&;

    auto Run() -> void;

    auto Stop() -> void;

    auto AddApiCallback(std::string path, std::function<std::string(HttpRequest)> callback) -> void;

private:
    std::atomic_bool m_run{true};

    auto listener() -> void;

    auto worker(size_t id) -> void;

    auto createListenFD() -> int;

    std::string m_port_number;
    std::filesystem::path m_file_directory;
    uint64_t m_number_of_threads;
    std::thread m_listener_thread;
    std::vector<std::thread> m_worker_threads;
    FileReader m_file_reader;
    SocketQueueCollection m_socket_queue_collection;

    std::unordered_map<std::string, std::function<std::string(HttpRequest)>> m_api_paths;
};

}
