#pragma once

#include "simple_server/HttpRequest/Headers.h"
#include "simple_server/HttpRequest/HttpRequestEnums.h"

#include <string>
#include <unordered_map>

namespace simple_server {
    class HttpRequest {
        using ParamMap = std::unordered_map<std::string, std::string>;
        public:
            auto ParseIncomingMessage(std::string_view incoming_message) -> void;
            auto GetHttpMethod() const -> HttpMethod;
            auto GetHttpPath() const -> const std::string&;
            auto GetHttpQueryParameterMap() const -> const ParamMap&;
            auto GetHttpQueryParameterString() const -> const std::string&;
            auto GetHttpVersion() const -> HttpVersion;
            auto GetHeaders() -> const Headers&;
            auto GetHeaders() const -> const Headers&;
            auto GetBody() const -> const std::optional<std::string>&;

        private:
            auto extractRequestLine(std::string_view message) -> size_t;
            auto extractPathAndQueryParam(std::string_view path_and_params) -> void;
            auto extractQueryParamMap(std::string_view query_params) -> void;
            auto extractHeaders(std::string_view message, size_t start_pos) -> size_t;
            auto extractBody(std::string_view message, size_t start_pos) -> void;
            HttpMethod  m_http_method{HttpMethod::INVALID};
            std::string m_http_path;
            std::string m_parameter_string;
            ParamMap    m_parameter_map;
            HttpVersion m_http_version;
            Headers     m_incoming_headers;
            std::optional<std::string> m_incoming_post_body;
    };
}
