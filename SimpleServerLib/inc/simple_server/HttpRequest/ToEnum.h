#include <string>

namespace simple_server {
// Necessary in order to allow for specialization, but not intended for actual use.
template<class T>
auto to_enum(std::string_view method_string) -> T;
}
