#pragma once

#include <string>
#include <unordered_map>

namespace simple_server {
class Headers {
public:
    auto AddHeader(std::string header, std::string content) -> void;

    auto AddHeaderFromString(std::string_view header_to_parse) -> void;

    auto GetHeaders() -> const std::unordered_map<std::string, std::string>&;
    auto GetHeaders() const -> const std::unordered_map<std::string, std::string>&;

private:
    std::unordered_map<std::string, std::string> m_headers;
};
}
