#pragma once

#include "simple_server/HttpRequest/ToEnum.h"

#include <string>
#include <iostream>

namespace simple_server {

// Based on Mozilla docs: https://developer.mozilla.org/ja/docs/Web/HTTP/Methods
enum class HttpMethod {
    GET,
    HEAD,
    POST,
    PUT,
    DELETE,
    CONNECT,
    OPTIONS,
    TRACE,
    PATCH,
    INVALID
};

template<>
auto to_enum<HttpMethod>(std::string_view method_string) -> HttpMethod;

auto to_string(HttpMethod http_method) -> const std::string&;

enum class HttpVersion {
    v0_9,
    v1_0,
    v1_1,
    v2_0,
    INVALID
};

template<>
auto to_enum<HttpVersion>(std::string_view method_string) -> HttpVersion;

auto to_string(HttpVersion http_version) -> const std::string&;

}
