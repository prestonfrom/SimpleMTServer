#pragma once

#include <condition_variable>
#include <mutex>
#include <vector>

namespace simple_server {

class AtomicSocketQueue {
public:
    explicit AtomicSocketQueue(size_t id, size_t max_size = 100);
    AtomicSocketQueue(const AtomicSocketQueue&) = delete;
    AtomicSocketQueue(AtomicSocketQueue&&);
    ~AtomicSocketQueue() = default;
    auto Add(int socket_fd) -> void;
    auto Pop() -> int;
    auto HasItem() -> bool;
    auto GetConditionVariable() -> std::condition_variable&;
    auto Stop() -> void;

private:
    auto resize() -> void;
    size_t m_id;
    std::vector<int> m_queue;
    size_t m_head{0};
    size_t m_tail{0};
    size_t m_max_size;
    std::mutex m_lock{};
    std::condition_variable m_condition_variable;
};

}
