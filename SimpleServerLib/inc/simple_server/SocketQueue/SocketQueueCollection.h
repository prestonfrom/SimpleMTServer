#pragma once

#include "simple_server/SocketQueue/AtomicSocketQueue.h"

#include <vector>
#include <memory>

namespace simple_server {

class SocketQueueCollection {
public:
    explicit SocketQueueCollection(size_t size);
    SocketQueueCollection(const SocketQueueCollection&) = delete;
    SocketQueueCollection(SocketQueueCollection&&) = default;
    ~SocketQueueCollection() = default;

    auto AddSocket(int socket_fd) -> void;
    auto GetQueue(size_t index) -> simple_server::AtomicSocketQueue&;
    auto Stop() -> void;
private:
    std::vector<simple_server::AtomicSocketQueue> m_collection;
    size_t m_current_queue{0};
};

}
