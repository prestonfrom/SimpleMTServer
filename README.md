<h2>SimpleMTServer</h2>

Implementation of a multi-threaded HTTP server implemented with raw Linux sockets and C++17. This was born out of curiousity and an inability to find a fully implemented example in modern C++. <b>This project is still very much a work in progress!</b>

<h4>Results of testing with [wrk2](https://github.com/giltene/wrk2) locally:</h4>

Ran using Intel i7-5600U with 16 GB of ram. In each instance, the example server was bound to port 8080 and was using 4 threads.

Using WRK2 with four threads with a total of 10,000 connections and a rate limit of 25,000 requests per second.
```
./wrk -c 10000 -d 10 -t 4 -R 25000 http://localhost:8080/subdirectory/subfile.html

Running 10s test @ http://localhost:8080/subdirectory/subfile.html
  4 threads and 10000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    37.44ms  157.43ms   2.69s    95.26%
    Req/Sec       -nan      -nan   0.00      0.00%
  98200 requests in 10.00s, 19.10MB read
Requests/sec:   9816.65
Transfer/sec:      1.91MB
```

Using WRK2 with one thread with a total of 10,000 connections and a rate limit of 25,000 requests per second.
```
./wrk -c 10000 -d 10 -t 1 -R 25000 http://localhost:8080/subdirectory/subfile.html

Running 10s test @ http://localhost:8080/subdirectory/subfile.html
  1 threads and 10000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.98ms    3.83ms  73.54ms   98.98%
    Req/Sec       -nan      -nan   0.00      0.00%
  25753 requests in 10.00s, 5.01MB read
Requests/sec:   2574.60
Transfer/sec:    512.91KB
```

Using WRK2 with four threads with a total of 100 connections and a rate limit of 25,000 requests per second.
```
./wrk -c 100 -d 10 -t 4 -R 25000 http://localhost:8080/subdirectory/subfile.html

Running 10s test @ http://localhost:8080/subdirectory/subfile.html
  4 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.00ms  640.74us  14.77ms   79.45%
    Req/Sec       -nan      -nan   0.00      0.00%
  248527 requests in 10.00s, 48.35MB read
Requests/sec:  24852.46
Transfer/sec:      4.84MB
```
