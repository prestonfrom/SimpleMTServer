# Build example executables

SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)

ADD_EXECUTABLE(create_simple_server create_simple_server.cpp)

TARGET_LINK_LIBRARIES(create_simple_server SimpleServer stdc++fs)

INCLUDE_DIRECTORIES(PRIVATE ${PROJECT_SOURCE_DIR}/SimpleServerLib/inc)

