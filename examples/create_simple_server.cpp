#include "simple_server/SimpleServer.h"

#include <iostream>
#include <filesystem>
#include <csignal>

std::atomic_int run{1};

void signalHandler(int signum) {
    std::cout << "Signal Received: " << signum << "\n";
    run.store(0);
}

int main(int argc, char** argv) {
    if (argc != 4) {
        std::cout << "Please specify port number, file directory, and number of worker threads to use.\n";
        return 0;
    }

    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGPIPE, SIG_IGN);

    std::cout << "Starting server...\n";

    uint64_t threads = std::stoull(argv[3]);
    simple_server::SimpleServer simple_mt_server(argv[1], argv[2], threads);
    std::cout << simple_mt_server.GetPortNumber() << " " << simple_mt_server.GetFileDirectory() << "\n";
    simple_mt_server.Run();

    std::cout << "Running until signal is received...\n";

    while (run > 0);

    std::cout << "Signal received! Shutting down...\n";

    simple_mt_server.Stop();

    return 0;
}
